import re
import sys
import collections

# Ivan Izmailov.

# Use dfs for checking graph for unreacheble|disconnected vertices.
def dfs(graph, state, visited):
    if state in visited:
        return;
    
    visited.add(state)
    
    for next in graph.get(state, []):        
        dfs(graph, next, visited)
    
    # Returns all vertices which were visited..
    return visited

# Only reads file and parses it.
def read_raw(file = 'fsa.txt'):
    # Remove spaces, split onto 'key' and 'values'.
    def parse_line(line):
        return line.replace(' ', '').strip().split('=', 2)
    
    # Split values by coma.
    def parse_values(line):
        return [value for value in line[1:-1].split(',')]

    # Parse line, returns (key, values)
    def parse(line):
        key, raw_values = parse_line(line)
        
        return (key, parse_values(raw_values) if len(raw_values) > 2 else [])
    
    # FSA.
    fsa_raw = {}
    
    # Open file and read it.
    with open(file) as file:
        # Use regular expression for validate input.
        regex = [r'states=\[([A-Za-z0-9]+,)*([A-Za-z0-9]+)\]\s*$', 
                 r'alpha=\[([A-Za-z0-9_]+,)*([A-Za-z0-9_]+)\]\s*$', 
                 r'init\.st=\[([A-Za-z0-9]+)*\]\s*$', 
                 r'fin\.st=\[(([A-Za-z0-9]+,)*([A-Za-z0-9]+))*\]\s*$', 
                 r'trans=\[((([A-Za-z0-9]+)>([A-Za-z0-9_]+)>([A-Za-z0-9]+),)*(([A-Za-z0-9]+)>([A-Za-z0-9_]+)>([A-Za-z0-9]+))+)*\]\s*$']
        
        for line in file:
            # If we cannot find such pattern.
            if re.match(regex.pop(0), line) == None:
                raise ValueError('E5: Input file is malformed')
            
            # Parse.
            key, values = parse(line)
            
            # Save.
            fsa_raw[key] = values
    
    # Return dictionary.
    return fsa_raw

# Read and validate fsa.        
def read_fsa():
    def validate(fsa):
        # If init state not in states.
        if not fsa.initial_state in fsa.states:
            raise ValueError("E1: A state '%s' is not in the set of states" % fsa.initial_state)
            
        # Checking for invalid states in final states.
        for state in fsa.final_states.difference(fsa.states):
            raise ValueError("E1: A state '%s' is not in the set of states" % state)
        
        # E2. Undirected graph.
        graph = {}
        
        # Go throught transition. Transition looks like (from, letter, to).
        for transition in fsa.transitions:
            # Checking for invalid states.
            if not transition[0] in fsa.states:
                raise ValueError("E1: A state '%s' is not in the set of states" % transition[0])
            
            # Checking for invalid states.
            if not transition[2] in fsa.states:
                raise ValueError("E1: A state '%s' is not in the set of states" % transition[2])
        
            # Checking for invalid letters.
            if not transition[1] in fsa.alphabet:
                raise ValueError("E3: A transition '%s' is not represented in the alphabet" % transition[1])
            
            # E2. Build undirected graph.
            if transition[0] in graph:
                graph[transition[0]].add(transition[2])
            else:
                graph[transition[0]] = set([transition[2]])
            
            # E2. Build undirected graph.
            if transition[2] in graph:
                graph[transition[2]].add(transition[0])
            else:
                graph[transition[2]] = set([transition[0]])
            
        # If visited by dfs vertices not equal all states, then we have disjoint.
        if dfs(graph, fsa.initial_state, set()) != fsa.states:
            raise ValueError('E2: Some states are disjoint')
        
        # Simple return the same fsa.
        return fsa
    
    def remove_duplicates(values):
        return list(collections.OrderedDict.fromkeys(values).keys())
    
    # FSA.  
    fsa = {}
    
    # Try read.
    try:
        fsa_raw = read_raw()
    # If smth goes wrong with file (Doesn't exists or permisions denied.)- error E5.
    except OSError as exc:
        raise OSError('E5: Input file is malformed')
    
    # For saving order.
    fsa['ordered'] = {'states':       remove_duplicates(fsa_raw['states']),
                      'final_states': remove_duplicates(fsa_raw['fin.st'])}
    
    # Making fsa more comfortable.
    fsa['states']        = frozenset(fsa_raw['states'])
    fsa['alphabet']      = frozenset(fsa_raw['alpha'])
    fsa['final_states']  = frozenset(fsa_raw['fin.st'])
    # Parse transitions. Transition looks like (from, letter, to).
    fsa['transitions']   = frozenset([tuple(arg for arg in transition_raw.split('>')) for transition_raw in fsa_raw['trans']])
    
    # If there is no init state.
    if len(fsa_raw['init.st']) != 1:
        raise ValueError('E4: Initial state is not defined')
    
    # We have only one init state.
    fsa['initial_state'] = fsa_raw['init.st'][0]
    
    # Validates and returns a new class with name 'FSA' and fields from 'fsa' dictionary. 
    # So now we can access by dot. I.e.: fsa.states = frozenset(1, 3, 4 ...).
    return validate(type('FSA', (), fsa))
    
  
def analyze_fsa(fsa):
    # Comlete/Incomplete.
    alphabet_per_state = {}
    
    # W3. Nondeterministic.
    unique_transitions_per_state = collections.Counter()
    
    # w2. Unreachable. Build directed graph for dfs.
    graph = {}
    
    for transition in fsa.transitions:
        # Comlete/Incomlete. Count alphabet per state to check comlete|incomplete.
        if transition[0] in alphabet_per_state:
            alphabet_per_state[transition[0]].add(transition[1])
        else:
            alphabet_per_state[transition[0]] = set([transition[1]])
    
        # W2. Build directed graph.
        if transition[0] in graph:
            graph[transition[0]].add(transition[2])
        else:
            graph[transition[0]] = set([transition[2]])
            
        # W3. Count unique transitions per state, to check nondeterministic.
        unique_transitions_per_state[(transition[0], transition[1])] += 1
    
    isComplete = True
    for state in alphabet_per_state:
        # If any state has alphabet which doesn't equal fsa's alphabet, then we have incomplete.
        if alphabet_per_state[state] != fsa.alphabet:
            isComplete = False
            break
    
    warnings = []
    
    # W1. If there are no final states.
    if len(fsa.final_states) == 0:
        warnings.append('W1: Accepting state is not defined')
    
    # W2. If vertices visited by dfs not equal state, then we have unreacheble states.
    if dfs(graph, fsa.initial_state, set()) != fsa.states:
        warnings.append('W2: Some states are not reachable from the initial state')
    
    # E6. If all transitions in unique - deterministic.
    if unique_transitions_per_state.most_common(1)[0][1]!= 1:
        raise ValueError('E6: FSA is nondeterministic')
    
    return (isComplete, warnings)

 
def get_regular_expression(fsa):
    # All states except initial state. In the same order as represented.
    ordered_states = filter(lambda x: 0 if x == fsa.initial_state else 1, fsa.ordered['states'])

    # Make a dictionary, which will keep correspondence between index and state. 
    state_to_index = dict([pair for pair in zip(ordered_states, range(1, len(fsa.states)))])
    
    # q0 should be initial_state.
    state_to_index[fsa.initial_state] = 0
    
    def get_index(state):
        return state_to_index[state]
    
    graph = {}
    
    # Build directed graph for easier work.
    for transition in fsa.transitions:
        from_  = get_index(transition[0])
        to     = get_index(transition[2])
        letter = transition[1]
    
        # I store a set of letters and transition targets from given state. 
        if from_ in graph:
            graph[from_].append((letter, to))
        else:
            graph[from_] = [(letter, to), ]
    
    def R(i, j, k):
        if k == -1:
            # Get transitions from state i and filter to get only transitions from i to j.
            transitions = filter(lambda item: 1 if item[1] == j else 0, graph[i])
        
            # Get letters from transitions.
            letters = [transition[0] for transition in transitions]
            
            letters.sort()
            
            if i == j:
                return '|'.join(letters + ['eps', ])
                
            elif len(letters) == 0:
                return '{}'
                
            return '|'.join(letters)
            
        return '(%s)(%s)*(%s)|(%s)' % (R(i, k, k-1), R(k, k, k-1), R(k, j, k-1), R(i, j, k-1))
    
    
    if len(fsa.final_states) == 0:
        return '{}'
    
    return '|'.join([R(0, get_index(final_state), len(fsa.states) - 1) for final_state in fsa.ordered['final_states']])
 
if __name__ == '__main__':
    fsa = None
    
    with open('result.txt', 'w') as file:
        # Try read and validate.
        try:      
            fsa = read_fsa()
            
            # Analize. Raise an error if nondeterministic.
            info = analyze_fsa(fsa)
            
        # If smth goes wrong.
        except (OSError, ValueError) as exc:
            print('Error:\n', exc, sep = '', file = file)
        else:
            print(get_regular_expression(fsa), file = file)
                